package backend

import "capture/internal/app"

type IntellIoTOptions struct {
	PacketRate int
	Throughput int
}

type Options struct {
	KitBackend      string
	TrustBackend    string
	ApiBackend      string
	AppStats        bool
	KitCountPackets bool
	PcapReplay      bool
	IntellIoTOpts   IntellIoTOptions
}

func NewOptions() *Options {
	app.Log.Trace("kit: new options")
	return &Options{
		KitBackend:      app.Cfg.AppBackendKit,
		TrustBackend:    app.Cfg.AppBackendTrust,
		ApiBackend:      app.Cfg.AppBackendApi,
		AppStats:        app.Cfg.AppShowStats,
		KitCountPackets: app.Cfg.KitCountPackets,
		PcapReplay:      app.Cfg.PcapReplay,
		IntellIoTOpts: IntellIoTOptions{
			PacketRate: app.Cfg.KitIntellIoTPacketRate,
			Throughput: app.Cfg.KitIntellIoTThroughput,
		},
	}
}

func (o Options) Copy() Options {
	app.Log.Trace("kit: copy options")
	return Options{
		KitBackend:      o.KitBackend,
		TrustBackend:    o.TrustBackend,
		ApiBackend:      o.ApiBackend,
		AppStats:        o.AppStats,
		KitCountPackets: o.KitCountPackets,
		PcapReplay:      o.PcapReplay,
		IntellIoTOpts: IntellIoTOptions{
			PacketRate: o.IntellIoTOpts.PacketRate,
			Throughput: o.IntellIoTOpts.Throughput,
		},
	}
}
