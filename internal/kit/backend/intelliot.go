package backend

import (
	"capture/internal/app"
	"capture/internal/trust"
	"fmt"
	"io"
	"sync"
	"time"

	"github.com/google/gopacket"
)

type IntellIoT struct {
	ch              chan IntellIoTFeature
	winSize         int
	packetThreshold int
	src             *gopacket.PacketSource
	opts            Options
	trust           trust.Trust
	trustLock       sync.Mutex
}

// chan []IntellIoTFeature
// or
// chan IntellIoTWindow
// struct IntellIoTWindow {...}

// TODO: create a structure that fits what we collect
type IntellIoTFeature struct {
	Timestamp   time.Time
	SrcIp       gopacket.Endpoint
	PayloadSize int
}

func (f IntellIoTFeature) IsValid() bool {
	var ep gopacket.Endpoint

	return ep != f.SrcIp
}

func NewIntellIoTFeature(p gopacket.Packet) IntellIoTFeature {
	var ts time.Time
	var src gopacket.Endpoint
	var size int

	ts = p.Metadata().Timestamp

	// ethernet, arp, etc
	if p.NetworkLayer() != nil {
		src = p.NetworkLayer().NetworkFlow().Src()
		size = len(p.NetworkLayer().LayerPayload())
	}

	return IntellIoTFeature{
		Timestamp:   ts,
		SrcIp:       src,
		PayloadSize: size,
	}
}

func NewIntellIoT(src *gopacket.PacketSource, opts *Options, trust trust.Trust) *IntellIoT {
	app.Log.Trace("kit (intelliot): new")

	iot := new(IntellIoT)

	iot.ch = make(chan IntellIoTFeature, 10)
	iot.winSize = 1000
	iot.packetThreshold = 5
	iot.src = src
	iot.opts = opts.Copy()
	iot.trust = trust

	app.Stats["kit.intelliot"] = iot

	return iot
}

func (iot *IntellIoT) Run(wg *sync.WaitGroup) {
	app.Log.Trace("kit (intelliot): run")
	wg.Add(1)

	go iot.accumulate()

	if iot.opts.PcapReplay {
		go iot.replay(wg)
	} else {
		go iot.read(wg)
	}
}

func (iot *IntellIoT) replay(wg *sync.WaitGroup) {
	app.Log.Trace("kit (intelliot): replay")

	app.Log.Info("kit: reader started")
	var prev, next time.Time
	for curr := range iot.src.Packets() {
		next = curr.Metadata().Timestamp
		app.Log.Tracef("next timestamp: %s\n", next.String())
		if !prev.IsZero() {
			diff := next.Sub(prev)
			app.Log.Tracef("next packet at: %s\n", diff)
			time.Sleep(diff)
		}

		feat := NewIntellIoTFeature(curr)

		if feat.IsValid() {
			app.Log.Tracef("kit (intelliot): packet from %s", feat.SrcIp.String())
			iot.ch <- feat
		} else {
			app.Log.Debug("kit (intelliot): skipped packet: lower than network layer")
		}

		prev = next
	}

	wg.Done()
	app.Log.Info("kit: reader finished")
	if iot.opts.AppStats {
		app.PrintStats()
	}
}

func (iot *IntellIoT) read(wg *sync.WaitGroup) {
	app.Log.Trace("kit (intelliot): read")

	app.Log.Info("kit: reader started")
	for curr := range iot.src.Packets() {
		feat := NewIntellIoTFeature(curr)

		if feat.IsValid() {
			app.Log.Tracef("kit (intelliot): packet from %s", feat.SrcIp.String())
			iot.ch <- feat
		} else {
			app.Log.Debug("kit (intelliot): skipped packet: lower than network layer")
		}
	}

	wg.Done()
	app.Log.Info("kit: reader finished")
	if iot.opts.AppStats {
		app.PrintStats()
	}
}

func (iot *IntellIoT) accumulate() {
	app.Log.Trace("kit (intelliot): accumulate")
	window := [][]IntellIoTFeature{}
	size := 0
	curr := 0

	window = append(window, make([]IntellIoTFeature, iot.winSize))

	// TODO: add timer also
	for {
		feat := <-iot.ch

		window[curr] = append(window[curr], feat)

		size++
		if size == iot.winSize {
			// TODO: check this works as expected
			go iot.process(window[curr])
			curr = curr + 1
			window = append(window, make([]IntellIoTFeature, iot.winSize))
			size = 0
		}
	}
}

func (iot *IntellIoT) process(data []IntellIoTFeature) {
	app.Log.Trace("kit (intelliot): process")
	winNodes := make(map[gopacket.Endpoint][]IntellIoTFeature)

	// split features per node
	for _, d := range data {
		// why?
		if d.PayloadSize == 0 {
			continue
		}
		winNodes[d.SrcIp] = append(winNodes[d.SrcIp], d)
	}

	var wg sync.WaitGroup

	// for each node
	// compute packetrate and throughput
	// check thresholds and either penalize or reward
	for node, packets := range winNodes {
		wg.Add(1)
		go iot.networkMetrics(&wg, node, packets)
	}

	wg.Wait()

	// publish trust values
	iot.trust.Update()
}

func (iot *IntellIoT) networkMetrics(wg *sync.WaitGroup, node gopacket.Endpoint, packets []IntellIoTFeature) {
	app.Log.Trace("kit (intelliot): networkMetrics")
	defer wg.Done()

	last := len(packets) - 1
	tic := packets[0].Timestamp
	toc := packets[last].Timestamp
	duration := float64(toc.Sub(tic).Nanoseconds())

	if duration < float64(time.Microsecond) {
		app.Log.Infof("kit (intelliot): skip %s, %f nsec", node.String(), duration)
		return
	}

	if len(packets) < iot.packetThreshold {
		app.Log.Infof("kit (intelliot): skip %s, %d packets", node.String(), len(packets))
		return
	}

	throughput := float64(0)
	packetrate := float64(len(packets)*1000000000) / duration

	for _, p := range packets {
		throughput += float64(p.PayloadSize)
	}

	throughput = throughput * 1000000 / duration

	app.Log.Infof("kit (intelliot): %s: packets=%d, duration=%f, packetrate=%f", node.String(), len(packets), duration, packetrate)
	app.Log.Tracef("kit (intelliot): %s: packets=%d, duration=%f, throughput=%f", node.String(), len(packets), duration, throughput)

	iot.trustLock.Lock()
	if packetrate > float64(iot.opts.IntellIoTOpts.PacketRate) {
		iot.trust.Penalize(node.String(), fmt.Sprintf("packet rate %f", packetrate))
	} else {
		iot.trust.Reward(node.String())
	}

	if throughput > float64(iot.opts.IntellIoTOpts.Throughput) {
		iot.trust.Penalize(node.String(), fmt.Sprintf("throughput %f", throughput))
	} else {
		iot.trust.Reward(node.String())
	}
	iot.trustLock.Unlock()
}

func (iot *IntellIoT) Report(w io.Writer) {
	app.Log.Trace("kit (intelliot): report stats")
}

// TODO: work with cross correlation
